Truckload

<h3> <b> Техническое задание по проекту “Truckload - системы эффективного использования ресурсов грузоперевозчиков”</b>
</h3>

<h3><b>1. Общие сведения</b></h3>

<h5>1.1 Наименование проекта</h5>
Проект Truckload AI.

<h5>1.2 Назначение проекта</h5>
В сфере коммерческих грузоперевозок автомобильным транспортом подбор заявок на грузоперевозки 
 осуществляется вручную диспетчерами экспедиторских компаний, поэтому возможна обработка 
 только ограниченного количества вариантов. Назначением данного проекта является создание 
 системы, которая позволит обрабатывать неограниченное количество заявок на перевозки, мы 
 ожидаем, что это позволит получить прирост в метриках эффективности.

<h5>1.3 Цели проекта</h5>
<table>
<tr>
<td>Цель проекта</td>
<td>Метрика</td>
</tr> 
<tr>
<td>Реализовать и протестировать алгоритмы, позволяющие рекомендовать заявки на грузоперевозку, 
наиболее эффективно использующие ресурс грузоперевозчика.</td>
<td>
<ol>
<li>Работоспособность решения подтверждается сопоставлением с ручным расчетом
<li>Качество алгоритма рекомендаций оценивается:
  <ol>
   <li>Повышением суммарной выручки грузоперевозчика за период не менее чем на 10%. В качестве периода берется не менее
1 календарного месяца;
   <li>Уменьшение времени простоя транспорта не менее чем на 7.5%;
   <li>Уменьшение порожних пробегов не менее чем на 30%.</li></ol></li></ol></td>
</tr>
</table>

Для того, чтобы рассчитать метрику 2.1, следует взять стоимость всех акцептованных грузоперевозок за заданный период.
<br>
Для того, чтобы рассчитать метрику 2.2, мы берем дату и время разгрузки и следующей за ней погрузки и рассчитываем время
между ними. Это время сравнивается с нормативом (4 часа). Суммарное время простоя за заданный период сравнивается с 
историческими данными (при ручном подборе заявок).<br>
Для того, чтобы рассчитать метрику 2.3 берем расстояние между точкой разгрузки и точкой следующей за ней погрузки. 
Суммарное расстояние порожнего пробега за заданный период сравнивается с историческими данными (при ручном подборе 
заявок).

<h5>1.4 Задачи проекта</h5>
В рамках проекта требуется выполнить следующие задачи:
<h5>1.4.1. Командой проекта:</h5>
<ul>
<li>Сбор и подготовка данных о коммерческих грузоперевозках:
<ul>
<li>Получение данных заявок о перевозках по разным API;</li>
<li>Получение данных о грузоперевозчиках и исторических данных их перевозок;</li>
</ul>
<li>Анализ и предобработка исходных, дополнительная разметка данных;
<li>Построение модели рекомендаций;
<li>Расчет метрик и сравнение;
<li>Валидация рекомендаций путем предложения грузоперевозчикам заявок;
<li>Разработка критериев оценки алгоритма в дополнение к метрикам;
<li>Определение дальнейших шагов развития проекта (конверсия и бэклог для цифрового продукта).</li>
</ul>

<h5>1.5 Результаты проекта</h5>
Результаты проекта:
<ol>
<li>Программный код алгоритма
<li>Итоговый отчёт, включающий в себя:
<ul>
<li>Результаты анализа и предобработки данных;
<li>Метрики качества работы алгоритмов и необходимых эвристик;
<li>Сравнение алгоритмов и описание наилучшего с точки зрения метрик решения;
<li>Границы применимости алгоритма;
<li>Рекомендации по доработке алгоритма;
<li>Бэклог на следующий этап работ;</li>
</ul>
<li>Демо-ролик работы алгоритма; 
<li>Заключения экспертов от бизнеса;
<li>Презентация с выводами по итогу проекта, рекомендациями и заключением по дальнейшему развитию проекта.</li>
</ol>

<h5>1.6 Сроки проекта</h5>
Результаты по проекту необходимо оформить в срок до <b>15.08.2023 г.</b>

<h3><b>2. Требования к документированию</b></h3>
По результатам реализации проекта необходимо предоставить:
<ol>
<li>Отчет, в формате .doсx;
<li>Демо-ролик, в формате .avi;
<li>Презентация об итогах реализации проекта, в формате .pptx.</li>
</ol>

==============================
Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">
cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
