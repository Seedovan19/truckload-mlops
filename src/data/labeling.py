"""
Код для разметки:
Парсятся грузы из чата 'Грузы Точка-Точка'.
И берется другой чат с сообщениями, не связанными
с грузоперевозками.
Данные из чата с заявками на грузоперевозку
размечаются в класс 1, а остальные
сообщения в класс 0.
"""

# -*- coding: utf-8 -*-
# pylint: disable=import-error

import logging  # type: ignore
import click  # type: ignore

from dotenv import find_dotenv, load_dotenv  # type: ignore
import pandas as pd  # type: ignore


@click.command()
@click.argument("file_cargos", type=click.STRING)
@click.argument("file_not_cargos", type=click.STRING)
@click.argument("output_filename", type=click.STRING)
def labeling(file_cargos: str, file_not_cargos: str, output_filename: str):
    """ Runs data processing scripts to turn raw data from (../external) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info("Создаем размеченный датасет")

    # Разметка сообщений с заявками на грузоперевозку
    cargo_input_file = pd.read_csv(file_cargos)
    cargo_input_file = cargo_input_file.assign(target=1)

    # Разметка остальных сообщений
    not_cargo_input_file = pd.read_csv(file_not_cargos)
    not_cargo_input_file = not_cargo_input_file.assign(target=0)

    print(not_cargo_input_file)

    # Соединение двух таблиц
    concat_df = pd.concat(
        [cargo_input_file, not_cargo_input_file], ignore_index=True
    ).dropna()
    concat_df = concat_df.sample(frac=1, random_state=42).reset_index(drop=True)

    concat_df.to_csv("data/processed/" + output_filename + ".csv", index=False)

    logger.info("Датасет успешно сохранен в директорию data/processed")


if __name__ == "__main__":
    LOG_FMT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=LOG_FMT)

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    # pylint: disable=E1120
    labeling()
