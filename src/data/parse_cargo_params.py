"""
Код парсит параметры грузов из канала
'Грузы Точка-Точка'
по регулярным выражениям
"""

# -*- coding: utf-8 -*-
# pylint: disable=import-error

import logging  # type: ignore
import re
import click  # type: ignore

from dotenv import find_dotenv, load_dotenv  # type: ignore
import pandas as pd  # type: ignore


@click.command()
def parse_cargo_params():
    """ Runs data processing scripts to turn raw data from (../external) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    data = []

    logger = logging.getLogger(__name__)
    logger.info("making final data set from raw data")

    messages = pd.read_csv("chats.csv")["message"].tolist()

    for message in messages:
        if not isinstance(message, str):
            continue

        row = {}
        cleaned_message = message.replace("Выгрузка ", "")

        row["Number"] = (
            re.search(r"№(\d+)", cleaned_message).group(1)
            if re.search(r"№(\d+)", cleaned_message)
            else None
        )

        row["Loading Date"] = (
            re.search(r"(\d{2}\.\d{2})", cleaned_message).group(1)
            if re.search(r"(\d{2}\.\d{2})", cleaned_message)
            else None
        )

        row["Loading Address"] = (
            re.search(r"(?:\d{2}\.\d{2}\s)(.*?)\s-\s", cleaned_message).group(1)
            if re.search(r"(?:\d{2}\.\d{2}\s)(.*?)\s-\s", cleaned_message)
            else None
        )

        row["Unloading Date"] = (
            re.search(r"-\s(\d{2}\.\d{2})", cleaned_message).group(1)
            if re.search(r"-\s(\d{2}\.\d{2})", cleaned_message)
            else None
        )

        row["Unloading Address"] = (
            re.search(r"(?:-\s\d{2}\.\d{2}\s)(.*)", cleaned_message).group(1)
            if re.search(r"(?:-\s\d{2}\.\d{2}\s)(.*)", cleaned_message)
            else None
        )

        row["Weight"] = (
            re.search(r"([\d.]+)т\.", cleaned_message).group(1)
            if re.search(r"([\d.]+)т\.", cleaned_message)
            else None
        )

        row["Loading Type"] = (
            re.search(r"([А-Я][а-я]+ погрузка)", cleaned_message).group(1)
            if re.search(r"([А-Я][а-я]+ погрузка)", cleaned_message)
            else None
        )

        row["Danger Class"] = (
            re.search(r"класс опасности - ([А-Я][а-я]+)", cleaned_message).group(1)
            if re.search(r"класс опасности - ([А-Я][а-я]+)", cleaned_message)
            else None
        )

        row["Rate with VAT"] = (
            re.search(r"Текущая ставка (\d+) с НДС", cleaned_message).group(1)
            if re.search(r"Текущая ставка (\d+) с НДС", cleaned_message)
            else None
        )

        row["Rate without VAT"] = (
            re.search(r"(\d+) без НДС", cleaned_message).group(1)
            if re.search(r"(\d+) без НДС", cleaned_message)
            else None
        )

        data.append(row)

    pd.DataFrame(data).to_csv("data/raw/parsed_cargos.csv", index=False)


if __name__ == "__main__":
    LOG_FMT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=LOG_FMT)

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    # pylint: disable=E1120
    parse_cargo_params()
