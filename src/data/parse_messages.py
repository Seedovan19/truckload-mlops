"""
Код парсит всю историю сообщений
в телеграм-канале
"""

# -*- coding: utf-8 -*-
# pylint: disable=import-error

import csv
import logging
import os
import click  # type: ignore

from telethon.sync import TelegramClient  # type: ignore
from dotenv import find_dotenv, load_dotenv  # type: ignore


def save_to_file(all_messages, filename, logger):
    """
    Сохраняем сообщения из Telegram
    в .csv-файл
    """
    logger.info("Сохраняем данные в файл")
    with open(
        "data/external/" + filename + ".csv", "w", encoding="UTF-8"
    ) as file_for_parsing:
        writer = csv.writer(file_for_parsing, delimiter=",", lineterminator="\n")

        writer.writerow(["messages"])

        for message in all_messages:
            writer.writerow([message])
        logger.info("Данные сохранены в файл data/external/%s.csv", filename)


@click.command()
@click.argument("tg_channel_link", type=click.STRING)
@click.argument("output_filename", type=click.STRING)
def parse_messages(tg_channel_link: str, output_filename: str):
    """
    Парсим сообщения из телеграмм-канала
    Для парсинга необходимо добавить в .env
    данные из личного кабинета telegram
    """
    phone = os.getenv("MY_PHONE")
    api_id = os.getenv("TG_API_ID")
    api_hash = os.getenv("TG_API_HASH")

    all_messages = []

    logger = logging.getLogger(__name__)
    logger.info("Парсинг сообщений из канала: %s", tg_channel_link)

    client = TelegramClient(phone, api_id, api_hash)
    client.start()

    entity = client.get_entity(tg_channel_link)
    history = client.get_messages(entity, limit=2000)

    for message_object in history:
        if not message_object.message:
            continue
        all_messages.append(message_object.message)

    save_to_file(all_messages, output_filename, logger)


if __name__ == "__main__":
    LOG_FMT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=LOG_FMT)

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    # pylint: disable=E1120
    parse_messages()
