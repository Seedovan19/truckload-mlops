"""
Обучаем модель классифицировать сообщения
из чатов: является ли сообщение
заявкой на грузоперевозку.

Используется модель BERT.
"""

# pylint: disable=import-error

import logging  # type: ignore
import pandas as pd  # type: ignore
from sklearn.model_selection import train_test_split  # type: ignore
from transformers import (  # type: ignore
    AutoTokenizer,
    AutoModelForSequenceClassification,
    Trainer,
    TrainingArguments,
)
import pyarrow as pa  # type: ignore
from datasets import Dataset  # type: ignore
import click  # type: ignore
import mlflow  # type: ignore

mlflow.set_experiment("lgbm1")


def process_data(row, tokenizer):
    """
    Обработка текстовых параметров
    в формат, понятный BERT 
    """
    text = row
    text = str(text)
    text = " ".join(text.split())

    encodings = tokenizer(text, padding="max_length", truncation=True, max_length=128)

    encodings["label"] = row["target"]
    encodings["text"] = text

    return encodings


@click.command()
@click.argument("number_of_messages", type=click.INT)
@click.argument("input_file", type=click.STRING)
def train_model(number_of_messages: int, input_file: str):
    """
    Подготовка параметров
    и дообучение модели BERT.
    С помощью MLFlow происходит логирование
    параметров, метрик и артефактов.
    """

    with mlflow.start_run():
        processed_data = []
        params = {
            "num_train_epochs": 3,
            "per_device_train_batch_size": 16,
            "per_device_eval_batch_size": 64,
            "learning_rate": 2e-5,
            "weight_decay": 0.01,
        }

        logger = logging.getLogger(__name__)
        logger.info("Дообучаем модель BERT на данных: %s", input_file)
        logger.info("Для обучения взято %s сообщений", str(number_of_messages))

        data = pd.read_csv(input_file)

        logger.info("Загрузка токенизатора BERT")

        for i in range(len(data[:number_of_messages])):
            processed_data.append(
                process_data(
                    data.iloc[i], AutoTokenizer.from_pretrained("bert-base-uncased")
                )
            )

        train_data, test_data = train_test_split(
            pd.DataFrame(processed_data), test_size=0.2, random_state=42
        )

        train_hg = Dataset(pa.Table.from_pandas(train_data))
        valid_hg = Dataset(pa.Table.from_pandas(test_data))

        model = AutoModelForSequenceClassification.from_pretrained(
            "bert-base-uncased", num_labels=2
        )

        training_args = TrainingArguments(
            output_dir="models",
            num_train_epochs=params["num_train_epochs"],
            per_device_train_batch_size=params["per_device_train_batch_size"],
            per_device_eval_batch_size=params["per_device_eval_batch_size"],
            learning_rate=params["learning_rate"],
            weight_decay=params["weight_decay"],
            logging_dir="reports",
        )

        trainer = Trainer(
            model=model,
            args=training_args,
            train_dataset=train_hg,
            eval_dataset=valid_hg,
        )

        logger.info("Запуск дообучения модели")
        trainer.train()

        logger.info("Оценка обученной модели")
        evaluation_results = trainer.evaluate()

        logger.info("Модель сохранена в models/")
        model.save_pretrained("models/")

    mlflow.log_params(params)
    mlflow.log_metrics(evaluation_results)
    mlflow.log_artifact("models/", artifact_path="trained_model")


if __name__ == "__main__":
    LOG_FMT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=LOG_FMT)

    # pylint: disable=E1120
    train_model()
