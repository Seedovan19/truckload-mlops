import src


if __name__=="__main__":
    cargo_tg_link = 'https://t.me/gruzytt'
    cargo_filename = 'cargo_messages'

    not_cargo_tg_link = 'https://t.me/productPiter'
    not_cargo_filename = 'not_cargo_messages'

    path_cargo_messages = 'data/external/cargo_messages.csv'
    path_not_cargo_messages = 'data/external/not_cargo_messages.csv'
    processed_filename = 'data_labeled'

    num_of_messages = 400
    processed_dataset_path = 'data/processed/data_labeled.csv'

    src.parse_messages(cargo_tg_link, cargo_filename)
    src.parse_messages(not_cargo_tg_link, not_cargo_filename)
    src.labeling(path_cargo_messages, path_not_cargo_messages, processed_filename)
    src.train_model(num_of_messages, processed_dataset_path)
    src.predict_model()

